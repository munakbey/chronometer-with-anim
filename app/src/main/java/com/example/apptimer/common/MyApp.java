package com.example.apptimer.common;

import android.app.Application;
import android.content.Context;
import com.example.apptimer.model.ExecuteDao;

public class MyApp extends Application {
    private static Context context;
    private static ExecuteDao executer;

    @Override
    public void onCreate() {
        super.onCreate();
        context=getApplicationContext();
    }

    public static Context getAppContext(){
        return MyApp.context;
    }

    public static ExecuteDao executer(){
        if(executer==null){
            executer=new ExecuteDao();
        }
        return  executer;
    }
}
