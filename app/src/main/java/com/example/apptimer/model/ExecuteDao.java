package com.example.apptimer.model;

import android.content.Context;
import android.os.AsyncTask;

import com.example.apptimer.presenter.PTimer;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.example.apptimer.common.MyApp.getAppContext;

public class ExecuteDao {
    private MTimerDao daoTimer;
    private Context mContext;


    public ExecuteDao() {
        mContext = getAppContext();
        daoTimer = TimerDB.getInstance(mContext).getMTimer();
    }

    public List<MTimer> listTimer() {
        List<MTimer> list=null;
        try {
            list = new GetAllCarAsyncTask().execute().get(5, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public void addTimer(MTimer mTimer) {
        try {
            new AddCarAsyncTask().execute(mTimer).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class GetAllCarAsyncTask extends AsyncTask<Void, Void, List<MTimer>>{
        @Override
        protected List<MTimer> doInBackground(Void... voids) {
            return daoTimer.timerList();
        }
    }

    private class AddCarAsyncTask extends AsyncTask<MTimer, Void, MTimer> {
        @Override
        protected MTimer doInBackground(MTimer... mTimers) {
            daoTimer.insertTimer(mTimers[0]);
            return null;
        }
    }
}
