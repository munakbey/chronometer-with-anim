package com.example.apptimer.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import java.io.Serializable;

@Entity
public class MTimer implements Serializable {
    @PrimaryKey(autoGenerate = true)
    public int id;
    private String startsDate;
    private String finishDate;
    private String startTime;
    private String finishTime;
    private int  elapsedTime;

    public MTimer(String startsDate, String finishDate, String startTime, String finishTime, int elapsedTime) {
        this.startsDate = startsDate;
        this.finishDate = finishDate;
        this.startTime = startTime;
        this.finishTime = finishTime;
        this.elapsedTime = elapsedTime;
    }

    public int getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(int elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStartsDate() {
        return startsDate;
    }

    public void setStartsDate(String startsDate) {
        this.startsDate = startsDate;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

}
