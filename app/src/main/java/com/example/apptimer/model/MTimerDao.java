package com.example.apptimer.model;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface MTimerDao {

    @Insert
    void insertTimer(MTimer timer);

    @Query("SELECT * FROM mtimer")
    List<MTimer> timerList();

}
