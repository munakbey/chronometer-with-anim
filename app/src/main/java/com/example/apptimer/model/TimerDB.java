package com.example.apptimer.model;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {MTimer.class}, version = 3 ,exportSchema = false)
public abstract class TimerDB extends RoomDatabase {
    private static TimerDB instance;

    public static synchronized  TimerDB getInstance(Context context){
        if(instance==null){
            instance= Room.databaseBuilder(context,
                    TimerDB.class,"mtimer")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    public abstract MTimerDao getMTimer();
}
