package com.example.apptimer.presenter;

import com.example.apptimer.common.MyApp;
import com.example.apptimer.model.MTimer;
import com.example.apptimer.model.MTimerDao;

import java.util.List;

public class PTimer {
    public List<MTimer> getTimerList(){
        return MyApp.executer().listTimer();
    }

    public void addTimer(MTimer mTimer){
        MyApp.executer().addTimer(mTimer);
    }
}
