package com.example.apptimer.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;

import com.example.apptimer.R;
import com.example.apptimer.model.MTimer;
import com.example.apptimer.presenter.PTimer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    PTimer pTimer=new PTimer();
    Button btnStart,btnFinish,btnReset;
    String starDate;
    String finishDate;
    DateFormat df;
    DateFormat dfTime;
    String startTime;
    String finishTime;
    Chronometer mchronometer;
    Animation atg,btgone,btgtwo,roundingalone;
    ImageView imgBgCircle,imgOk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        definings();
        btnFinish.setVisibility(View.GONE);
        btnReset.setVisibility(View.GONE);

        df = new SimpleDateFormat("dd MMM yyyy");
        dfTime = new SimpleDateFormat("HH:mm:ss");

        imgBgCircle.startAnimation(btgone);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                starDate = df.format(Calendar.getInstance().getTime());
                startTime=starDate+dfTime.format(Calendar.getInstance().getTime());
                mchronometer.start();
                imgOk.startAnimation(roundingalone);
                btnFinish.setVisibility(View.VISIBLE);
                btnReset.setVisibility(View.VISIBLE);
            }
        });

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishDate = df.format(Calendar.getInstance().getTime());
                finishTime=dfTime.format(Calendar.getInstance().getTime());
                mchronometer.stop();

                long elapsedMillis = SystemClock.elapsedRealtime() - mchronometer.getBase();
                pTimer.addTimer(new MTimer(starDate,finishDate,startTime,finishTime,(int)((elapsedMillis-1)*0.001)));

                for (int i = 0; i <pTimer.getTimerList().size(); i++) {
                    Log.e("control",pTimer.getTimerList().get(i).getStartTime()+"--"+pTimer.getTimerList().get(i).getFinishTime());
                }
                imgOk.animate().cancel();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                starDate = df.format(Calendar.getInstance().getTime());
                mchronometer.setBase(SystemClock.elapsedRealtime());
                mchronometer.stop();
                imgOk.clearAnimation();
            }
        });

    }

    private void definings(){
        btnStart=findViewById(R.id.btn_start);
        btnFinish=findViewById(R.id.btn_finish);
        btnReset=findViewById(R.id.btn_reset);
        mchronometer=findViewById(R.id.mchronometer);
        atg= AnimationUtils.loadAnimation(this,R.anim.atg);
        btgone= AnimationUtils.loadAnimation(this,R.anim.btgone);
        btgtwo= AnimationUtils.loadAnimation(this,R.anim.btgtwo);
        roundingalone= AnimationUtils.loadAnimation(this,R.anim.roundingalone);
        imgBgCircle =findViewById(R.id.img_bgcircle);
        imgOk=findViewById(R.id.img_ok);
    }



}

